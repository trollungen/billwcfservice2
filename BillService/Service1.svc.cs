﻿using BillService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BillService
{
    public class Service1 : IService1
    {
        private BillDBEntities context;
        public Service1()
        {
            context = new BillDBEntities();
        }

        public bool AddCustomer(CustomerModel customer)
        {
            try
            {
                context.Customers.Add(new Customer
                {
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Address = customer.Address,
                    City = customer.City,
                });
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddProduct(ProductModel product)
        {
            try
            {
                context.Products.Add(new Product
                {
                    ProductName = product.ProductName,
                    Price = product.Price,
                });
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddOrderProduct(OrderProductModel order)
        {
            try
            {
                context.OrderProducts.Add(new OrderProduct
                {
                    OrderNr = order.OrderNr,
                    CustomerId = order.CustomerId,
                    ProductName = order.ProductName,
                    Qty = order.Qty,
                    Status = order.Status,
                    UnitPrice = order.UnitPrice,
                });
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool CreateInvoice(InvoiceModel invoice)
        {
            try
            {
                context.Invoices.Add(new Invoice
                {
                    FullName = invoice.Fullname,
                    Address = invoice.Address,
                    City = invoice.City,
                    OrderNr = invoice.OrderNr,
                    Created = DateTime.Now,
                    InvoiceNr = invoice.InvoiceNr,
                });
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public List<OrderProductModel> GetListOfAllOrders()
        {
            return context.OrderProducts.Select(list => new OrderProductModel
            {
                Id = list.Id,
                CustomerId = list.CustomerId,
                OrderNr = list.OrderNr,
                ProductName = list.ProductName,
                UnitPrice = list.UnitPrice,
                Qty = list.Qty,
                Status = list.Status,
            }).ToList();
        }

        public List<ProductModel> GetAllProducts()
        {
            return context.Products.Select(list => new ProductModel
            {
                Id = list.Id,
                ArticleId = list.ArticleId,
                Description = list.Description,
                Price = list.Price,
                ProductName = list.ProductName,
            }).ToList();
        }

        public List<CustomerModel> GetAllCustomers()
        {
            return context.Customers.Select(list => new CustomerModel
            {
                Id = list.Id,
                FirstName = list.FirstName,
                LastName = list.LastName,
                Address = list.Address,
                City = list.City,
            }).ToList();
        }

        public List<InvoiceModel> GetAllInvoice()
        {
            return context.Invoices.Select(list => new InvoiceModel
            {
                Id = list.Id,
                OrderNr = list.OrderNr,
                InvoiceNr = list.InvoiceNr,
                Fullname = list.FullName,
                Address = list.Address,
                City = list.City,
                Created = list.Created,
            }).ToList();
        }

        public CustomerModel GetCustomer(int id)
        {
            var getcustomer = context.Customers.FirstOrDefault(u => u.Id == id);
            if (getcustomer == null)
            {
                return null;
            }

            return new CustomerModel
            {
                Id = getcustomer.Id,
                FirstName = getcustomer.FirstName,
                LastName = getcustomer.LastName,
                Address = getcustomer.Address,
                City = getcustomer.City
            };

        }

        public ProductModel GetProdoct(int id)
        {
            var getproduct = context.Products.FirstOrDefault(u => u.Id == id);
            if (getproduct == null)
            {
                return null;
            }

            return new ProductModel
            {
                Id = getproduct.Id,
                ArticleId = getproduct.ArticleId,
                Description = getproduct.Description,
                Price = getproduct.Price,
                ProductName = getproduct.ProductName,
            };

        }

        public bool DeleteProduct(int id)
        {
            try
            {
                var product = context.Products.FirstOrDefault(p => p.Id == id);

                context.Products.Remove(product);
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateCustomer(CustomerModel customer)
        {
            if (customer != null)
            {
                var updateCutomer = context.Customers.FirstOrDefault(i => i.Id == customer.Id);

                updateCutomer.FirstName = customer.FirstName;
                updateCutomer.LastName = customer.LastName;
                updateCutomer.Address = customer.Address;
                updateCutomer.City = customer.City;
                updateCutomer.FullName = customer.FirstName + " " + customer.LastName;
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateProduct(ProductModel product)
        {
            try
            {
                var updateProduct = context.Products.FirstOrDefault(i => i.Id == product.Id);

                updateProduct.ProductName = product.ProductName;
                updateProduct.Price = product.Price;
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool UpdateAllPrices(decimal number)
        {
            try
            {
                var listofProduct = from list in context.Products
                                    where list.Id > 0
                                    select list;

                foreach (var product in listofProduct)
                {
                    product.Price = product.Price * number;
                }

                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void UpdateOrderStatus(bool status)
        {
            if (status)
            {
                var listoforders = from list in context.OrderProducts
                                   where list.Status == 0
                                   select list;
                foreach (var item in listoforders)
                {
                    item.Status = 1;
                }
                context.SaveChanges();
            }
        }
    }
}
