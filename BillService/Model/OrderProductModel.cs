﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BillService.Model
{
    public class OrderProductModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public int OrderNr { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public int Qty { get; set; }

        [DataMember]
        public decimal UnitPrice { get; set; }
    }
}