﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BillService.Model
{
    public class InvoiceModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public int InvoiceNr { get; set; }

        [DataMember]
        public int OrderNr { get; set; }

        [DataMember]
        public string Fullname { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string City { get; set; }
    }
}