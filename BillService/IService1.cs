﻿using BillService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BillService
{
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        List<ProductModel> GetAllProducts();

        [OperationContract]
        List<CustomerModel> GetAllCustomers();

        [OperationContract]
        List<InvoiceModel> GetAllInvoice();

        [OperationContract]
        List<OrderProductModel> GetListOfAllOrders();

        [OperationContract]
        CustomerModel GetCustomer(int id);

        [OperationContract]
        ProductModel GetProdoct(int id);

        [OperationContract]
        bool AddCustomer(CustomerModel customer);

        [OperationContract]
        bool AddProduct(ProductModel product);

        [OperationContract]
        bool AddOrderProduct(OrderProductModel order);

        [OperationContract]
        bool CreateInvoice(InvoiceModel invoice);

        [OperationContract]
        bool DeleteProduct(int id);

        [OperationContract]
        bool UpdateCustomer(CustomerModel customer);

        [OperationContract]
        bool UpdateProduct(ProductModel product);

        [OperationContract]
        bool UpdateAllPrices(decimal number);

        [OperationContract]
        void UpdateOrderStatus(bool status);
    }
}
